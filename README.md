# Tappit
Can you tappit as fast as you can before time runs out? Let's find out!

### CURRENT VERSION:
###### v2.2.5-beta

Runs on Android, iOS coming soon.
APK files can be found under "Releases".

***

This is my first project created in the Corona SDK.
It was part of my learning experience for my co-op placement.
While I did learn a lot in the creation of this, it is not very clean code.
I just added in things I wanted along the way with no real plan, and it shows.
The code works, but as a warning it is very messy.
I might plan on completely remaking the game with more organization, but that's for another day.
